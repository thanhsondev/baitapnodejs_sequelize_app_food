import express from "express";
import { getVideo } from "../controllers/videoController.js";


const videoRoute = express.Router()

videoRoute.get("/get-video", getVideo)

export default videoRoute