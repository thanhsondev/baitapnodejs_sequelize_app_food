import express from 'express'
import videoRoute from './videoRoute.js'
import restaurantRoute from './restaurantRouter.js'

const rootRouter = express.Router()

rootRouter.use("/video", videoRoute)

rootRouter.use("/restaurant",restaurantRoute )

export default rootRouter