import express from 'express'
import { comment, getCommentRes, getCommentUser, getLikeRes, getLikeUser, likeRes, unlikeRes, userOrder } from '../controllers/restaurantController.js'


const restaurantRoute = express.Router()


//like
restaurantRoute.post("/like", likeRes)
//unlike 
restaurantRoute.delete("/unlike", unlikeRes)
//getLike theo nhà hàng
restaurantRoute.get("/get-like-res", getLikeRes)
//getLike theo user
restaurantRoute.get("/get-like-user", getLikeUser)
// Thêm đánh giá
restaurantRoute.post("/comment", comment)
// getComment từ nhà hàng
restaurantRoute.get("/get-comment-res", getCommentRes)
//getComment từ user
restaurantRoute.get("/get-commnet-user", getCommentUser)
//Thêm order
restaurantRoute.post("order", userOrder)
export default restaurantRoute