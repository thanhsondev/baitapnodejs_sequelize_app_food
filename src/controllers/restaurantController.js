import sequelize from "../models/connect.js"
import initModels from "../models/init-models.js"

let model = initModels(sequelize)

// Xử lý like
const likeRes = async (req, res) => {
    let {res_id} = req.body
    let {user_id} = req.body
    let newData = {
        date_like: new Date(),
        user_id: user_id,
        res_id:res_id
    }
    let data = await model.like_res.create(newData)
    res.send("Đã like thành công" )
}

//Xử lý unlike
const unlikeRes = async (req, res) => {

    let {res_id, user_id} = req.body

        await model.like_res.destroy({
            where: {
                res_id,
                user_id
            }
        })

    res.send("Xóa thành công")
}

//Lấy danh sách like theo nhà hàng

const getLikeRes =async (req, res) => {
    let data = await model.like_res.findAll({
        include: ["re"]
    })

    res.send(data)
}

//Lấy danh sách like theo User

const getLikeUser =async (req, res) => {
    let data = await model.like_res.findAll({
        include: ["user"]
    })

    res.send(data)
}

//Thêm đánh giá

const comment = async (req, res) => {
    let {res_id, user_id, image, amount, descr, res_name} = req.body

    let dataRes = {
        res_name: res_name,
        image: image,
        descr: descr
    }
    await model.restaurant.create(dataRes)
    
    let checkResId = await model.restaurant.findAll({
        where: {
            res_id: res_id
        }
    })

    if(checkResId) {
        let dataRateRes = {
            amount: amount,
            date_rate: new Date(),
            res_id: res_id,
            user_id: user_id
        }
        await model.rate_res.create(dataRateRes)
    }
    res.send("thêm thành công")
}

// Lấy danh sách đánh giá từ nhà hàng
const getCommentRes = async(req, res) => {
    let data = await model.rate_res.findAll({
        include: ["re"]
    })
    res.send(data)
}

// Lấy danh sách đánh giá từ User
const getCommentUser = async(req, res) => {
    let data = await model.rate_res.findAll({
        include: ["user"]
    })
    res.send(data)
}

//Order

const userOrder =async (req, res) => {
    let {user_id, food_id, amount, code, arr_sub_id} = req.body

    let newData = {
        user_id: user_id,
        food_id: food_id,
        amount: amount,
        code: code,
        arr_sub_id :arr_sub_id
    }

    let data = await model.orders.create(newData)
    res.send(data)
}

export {
    likeRes,
    unlikeRes,
    getLikeRes,
    getLikeUser,
    comment,
    getCommentRes,
    getCommentUser,
    userOrder
}